package calc

import "calcFunc"

type Operators struct{
  Op1 int
  Op2 int
}

func (o Operators )Sum() int {
  return calcFunc.Suma (o.Op1, o.Op2)
}

func (o Operators )Subst() int {
  return calcFunc.Resta (o.Op1, o.Op2)
}

func (o Operators )Multi() int {
  return calcFunc.Multiplica (o.Op1, o.Op2)
}

func (o Operators )Div() float64 {
  return calcFunc.Divide (float64(o.Op1), float64(o.Op2))
}
