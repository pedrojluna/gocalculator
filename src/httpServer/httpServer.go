package main

import (
    "fmt"
    "calc"
    "net/http"
    "bytes"
    "io/ioutil"
    "html/template"
    
)


type Page struct {
  Title string
  Author string
  Header string
  Subheader string
  PageDescription string
  Content string
  URI1 string
  URI2 string
}

func calculate (w http.ResponseWriter, r *http.Request){
  myCalculator:= new (calc.Operators)
  myCalculator.Op1=2
  myCalculator.Op2=6
  //fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
  fmt.Fprintf(w, "The result from adding %d to %d is %d \n", myCalculator.Op1,myCalculator.Op2, myCalculator.Sum())
  fmt.Fprintf(w,"The result from substract %d to %d is %d \n", myCalculator.Op2,myCalculator.Op1, myCalculator.Subst())
  fmt.Fprintf(w,"The result from multiply %d times %d is %d \n", myCalculator.Op1,myCalculator.Op2, myCalculator.Multi())
  fmt.Fprintf(w,"The result from divide %d over %d is %f \n", myCalculator.Op1,myCalculator.Op2, myCalculator.Div())
}


func loadFile (fileName string) (string, error){
  bytes, err := ioutil.ReadFile(fileName)
  if err!=nil{
    return "", err
  }
  return string (bytes),nil
}

func index(w http.ResponseWriter, r *http.Request){
  var builder bytes.Buffer
  w.Header().Set("Content-Type", "text/html; charset=utf-8")
  builder.WriteString("Hi! my name is Pedro Luna and this site is a sampler of my technical abilities. 	<br> ")
  builder.WriteString("http server runung in go")
  builder.WriteString("The webpage is an html template")
  builder.WriteString("The content of the web page is delivered using go")
  builder.WriteString("instance on AWS")
  builder.WriteString("sourcecode is available in gitlab ")
  builder.WriteString("styles taken form w3schools")

  //fmt.Fprintf(w, "Welcome")
  page := new (Page)
  page.Title= "Peter's cherry lips sample page"
  page.Author="Pedro Luna"
  page.Header="Go baby Go Go!"
  page.Subheader="Yeah, we're looking at you!"
  page.PageDescription="Simple webpage written in Go lang"
  page.Content= builder.String()
  page.URI1="cv"
  page.URI2="calculator"
  //fmt.Fprintf(w, page.Title)
  t, _ :=template.ParseFiles("../../static/index.html")
  t.Execute(w,page)
}

func main (){
  http.HandleFunc("/calculator", calculate)
  http.HandleFunc("/", index)
/*
  fs := http.FileServer(http.Dir("static/"))
  http.Handle("/static/", http.StripPrefix("/static/", fs))
*/
  fs := http.FileServer(http.Dir("../../static/assets/"))
  http.Handle("/assets/", http.StripPrefix("/assets/", fs))
  fs1 := http.FileServer(http.Dir("../../static/documents/"))
  http.Handle("/documents/", http.StripPrefix("/documents/", fs1))


  http.ListenAndServe(":80", nil)


}
